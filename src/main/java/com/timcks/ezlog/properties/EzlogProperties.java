package com.timcks.ezlog.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "ezlog")
public class EzlogProperties {

    private static final String DEFAULT_FORMAT = "%2$s() %1$s: %3$s";

    private String formatUuid = "%s";
    private String formatBeforeMethod = DEFAULT_FORMAT;
    private String formatAfterMethod = DEFAULT_FORMAT;
    private String formatThrownMethod = DEFAULT_FORMAT;
    
}
