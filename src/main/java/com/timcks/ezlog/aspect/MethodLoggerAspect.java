package com.timcks.ezlog.aspect;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.timcks.ezlog.properties.EzlogProperties;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MethodLoggerAspect {

    public static final String METHOD_START = "START";
    public static final String METHOD_ERROR = "ERROR";
    public static final String METHOD_END = "END";

    @Autowired
    private Gson gson;
    @Autowired
    private EzlogProperties properties;

    private Map<String, Object> getArgs(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String[] argNames = methodSignature.getParameterNames();
        Object[] argValues = joinPoint.getArgs();
        Map<String, Object> args = new HashMap<>();
        if (argNames.length != argValues.length) {
            return args;
        }
        for (int i = 0; i < argValues.length; i++) {
            args.put(argNames[i], argValues[i]);
        }
        return args;
    }

    @Around("@within(com.timcks.ezlog.annotation.MethodLogger)")
    public Object aroundAnnotatedClassMethod(ProceedingJoinPoint joinPoint) throws Throwable {
        // Get info on annotated method
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String methodName = methodSignature.getName();
        Map<String, Object> methodArgs = getArgs(joinPoint);
        // Prepare logger
        Logger logger = LoggerFactory.getLogger(methodSignature.getDeclaringType());
        // Log BEFORE method
        Map<String, Object> logInfo = new HashMap<>();
        logInfo.put("args", methodArgs);
        String logInfoJSON = gson.toJson(logInfo);
        String logMessageBefore = String.format(properties.getFormatBeforeMethod(), METHOD_START, methodName,
                logInfoJSON);
        logger.info(logMessageBefore);
        // Record start time
        long startTime = System.nanoTime();
        // Let method proceed
        try {
            Object result = joinPoint.proceed();
            // Record end time
            long endTime = System.nanoTime();
            // Record execution time
            long executionTime = endTime - startTime;
            // Log AFTER method
            logInfo.put("execution time", String.format("%.3fs", (double) executionTime / 1e9));
            if (null != result) {
                logInfo.put("return", result);
            }
            logInfoJSON = gson.toJson(logInfo);
            String logMessageAfter = String.format(properties.getFormatAfterMethod(), METHOD_END, methodName,
                    logInfoJSON);
            logger.info(logMessageAfter);
            // Return result
            return result;
        } catch (Exception e) {
            // Record error time
            long errorTime = System.nanoTime();
            // Record execution time
            long executionTime = errorTime - startTime;
            // Log THROWN method
            logInfo.put("execution time", String.format("%.3fs", (double) executionTime / 1e9));
            logInfoJSON = gson.toJson(logInfo);
            String logMessageThrown = String.format(properties.getFormatThrownMethod(), METHOD_ERROR, methodName,
                    logInfoJSON);
            logger.error(logMessageThrown, e);
            // Rethrow error
            throw e;
        }
    }
    
}
