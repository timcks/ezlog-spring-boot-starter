package com.timcks.ezlog.aspect;

import java.util.UUID;

import com.timcks.ezlog.properties.EzlogProperties;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(1)
@Aspect
@Component
public class RequestUuidAspect {

    private static final String MDC_UUID = "UUID";

    @Autowired
    private EzlogProperties properties;

    private void setUUID() {
        MDC.put(MDC_UUID, String.format(properties.getFormatUuid(), UUID.randomUUID().toString()));
    }

    private void clearUUID() {
        MDC.remove(MDC_UUID);
    }

    @Before("@within(com.timcks.ezlog.annotation.RequestUuid)")
    public void beforeAnnotatedClassMethod() {
        setUUID();
    }

    @After("@within(com.timcks.ezlog.annotation.RequestUuid)")
    public void afterAnnotatedClassMethod() {
        clearUUID();
    }
    
}
